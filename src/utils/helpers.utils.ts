import fs from "fs/promises";

export const generateReqId = () => {
    return Math.random().toString(36).slice(2);
};

export const checkIfExists = async (path: string) => {
    try {
        await fs.access(path);
        return true;
    } catch (err) {
        return false;
    }
};
