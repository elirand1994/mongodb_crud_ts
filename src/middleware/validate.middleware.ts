import Joi from "joi";
import { Request, Response, NextFunction } from "express";
import { POST_METHOD, PUT_METHOD } from "../utils/constants.utils.js";
import ValidationException from "../exceptions/ValidationException.exceptions.js";

export const SchemaValidation = async (
    req: Request,
    res: Response,
    next: NextFunction
) => {
    let first_name = Joi.string().alphanum().min(0).max(30);
    let last_name = Joi.string().alphanum().min(0).max(30);
    let email = Joi.string().email({
        minDomainSegments: 2,
        tlds: { allow: ["com", "net"] },
    });
    let phone = Joi.string().length(10).regex(/^\d+$/);

    switch (req.method) {
        case POST_METHOD:
            first_name = first_name.required();
            last_name = last_name.required();
            email = email.required();
            phone = phone.required();
            break;
        case PUT_METHOD:
            break;
    }
    const schema = Joi.object().keys({ first_name, last_name, email, phone });
    let result;
    try {
        result = await schema.validateAsync(req.body);
        next();
    } catch (err:unknown) {
        next(new ValidationException((err as Error).message));
    }
};
