import express from "express";
import cors from "cors";
import morgan from "morgan";
import { connect_db } from "../db/mongoose.connection.js";
import user_router from "../modules/user/user.router.js";
import initMiddlewares from "../middleware/initializers.middlewares.js";
import errorMiddlewares from "../middleware/errors.middlewares.js";
class App {
    private app: express.Application;

    constructor() {
        // Setting up the app
        this.app = express();
        this.initializer();
    }

    private initializer = (): void => {
        this.app.use(express.json());
        this.app.use(express.urlencoded({ extended: true }));
        this.app.use(morgan("dev"));
        this.app.use(cors());

        this.app.use(initMiddlewares.ReqLogMiddleware);
        this.app.use("/api/users", user_router);
        this.app.use(errorMiddlewares.NotFoundMiddlewware);
        this.app.use(errorMiddlewares.WriteToErrorLogFileMiddleware);
        this.app.use(errorMiddlewares.ErrorResponseMiddleWare);
    };

    public start = async (): Promise<void> => {
        await connect_db("mongodb://localhost:27017/crud-demo");
        await this.app.listen(8080, "localhost");
        //log.magenta(`api is live on`,` ✨ ⚡  http://${HOST}:${PORT} ✨ ⚡`);
    };
}

const app = new App();
export default app;
