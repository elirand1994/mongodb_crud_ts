import HttpException from "./Http.exceptions.js";

class ResourceNotFoundException extends HttpException {
    constructor(path: string) {
        super(404, `URL with path ${path} not found!`);
    }
}

export default ResourceNotFoundException;
