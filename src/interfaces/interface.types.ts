export interface GeneralResponse {
    [key: string]: any;
}
export interface ResponseObj extends GeneralResponse {
    code: number;
    message: string;
    data?: any;
}

export interface ErrorResponse extends GeneralResponse {
    code: number;
    message: string;
    stack?: string;
}
