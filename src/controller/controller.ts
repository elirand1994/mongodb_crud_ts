import { Request, Response } from "express";
import userService from "../service/user.service.js";
class Controller {
    getPaginage = async (req: Request, res: Response) => {
        const page = Number(req.params.page);
        const to_limit = Number(req.params.limit);
        const response = await userService.paginateService(page, to_limit);
        res.status(response.code).json(response);
    };
    getAllUsers = async (req: Request, res: Response) => {
        const response = await userService.getAllUsersSerivce();
        res.status(response.code).json(response);
    };

    getUserById = async (req: Request, res: Response) => {
        const response = await userService.getUserByIdService(req.params.id);
        res.status(response.code).json(response);
    };

    deleteUserById = async (req: Request, res: Response) => {
        const response = await userService.deleteUserByIdService(req.params.id);
        console.log(response);
        res.status(response.code).json(response);
    };

    updateUserById = async (req: Request, res: Response) => {
        const response = await userService.updateUserByIdService(
            req.params.id,
            req.body
        );
        res.status(response.code).json(response);
    };

    createUser = async (req: Request, res: Response) => {
        const response = await userService.createUserService(req.body);
        res.status(response.code).json(response);
    };
}

const controller = new Controller();
export default controller;
