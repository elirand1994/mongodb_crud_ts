import { ErrorResponse, ResponseObj } from "../interfaces/interface.types.js";
import user_model from "../modules/user/user.model.js";
import * as UTILS from "../utils/constants.utils.js";
import mongoose from "mongoose";

class UserService {
    createUserService = async (body: any) => {
        const user = await user_model.create(body);
        console.log(user);
        const response: ResponseObj = {
            code: 200,
            message: `User with id ${user._id} has been created!`,
            data: user,
        };
        return response;
    };
    paginateService = async (page: number, to_limit: number) => {
        const to_skip = page * UTILS.USERS_PER_PAGE;
        const result = await user_model
            .find()
            .skip(to_skip)
            .limit(to_limit as number);
        const response: ResponseObj = {
            code: 200,
            message: `${to_limit} users from page ${page} have been loaded!`,
            data: result,
        };
        return response;
    };

    getAllUsersSerivce = async () => {
        const result = await user_model.find();
        const response: ResponseObj = {
            code: 200,
            message: "All users have been loaded.",
            data: result,
        };
        return response;
    };

    getUserByIdService = async (id: string) => {
        const given_id = mongoose.Types.ObjectId.isValid(id);
        if (!given_id) {
            const errResponse: ErrorResponse = {
                code: 403,
                message: `User with id ${id} has not been found!`,
            };
            return errResponse;
        }
        const user = await user_model.findById(id);
        if (!user) {
            const errResponse: ErrorResponse = {
                code: 403,
                message: `User with id ${id} has not been found!`,
            };
            return errResponse;
        } else {
            const response: ResponseObj = {
                code: 200,
                message: `User with id ${id} has been loaded`,
                data: user,
            };
            return response;
        }
    };

    deleteUserByIdService = async (id: string) => {
        const given_id = mongoose.Types.ObjectId.isValid(id);
        if (!given_id) {
            const errResponse: ErrorResponse = {
                code: 403,
                message: `User with id ${id} has not been found!`,
            };
            return errResponse;
        } else {
            const user = await user_model.findByIdAndDelete(id);
            if (user) {
                const response: ResponseObj = {
                    code: 200,
                    message: `User with id ${given_id} has been deleted.`,
                };
                return response;
            } else {
                const response: ResponseObj = {
                    code: 403,
                    message: `User with id ${given_id} has not been found!.`,
                };
                return response;
            }
        }
    };
    updateUserByIdService = async (id: string, body: any) => {
        const given_id = mongoose.Types.ObjectId.isValid(id);
        if (!given_id) {
            const errResponse: ErrorResponse = {
                code: 403,
                message: `User with id ${id} has not been found!`,
            };
            return errResponse;
        }
        const user = await user_model.findByIdAndUpdate(id, body);
        if (!user) {
            const errResponse: ErrorResponse = {
                code: 403,
                message: `User with id ${id} has not been found!`,
            };
            return errResponse;
        } else {
            const response: ResponseObj = {
                code: 200,
                message: `User with id ${given_id} has been deleted.`,
                data: user,
            };
            return response;
        }
    };
}

const userService = new UserService();
export default userService;
