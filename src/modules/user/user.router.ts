import raw from "../../middleware/route.async.wrapper.js";
import express from "express";
import controller from "../../controller/controller.js";
import { SchemaValidation } from "../../middleware/validate.middleware.js";

const router = express.Router();
// parse json req.body on post routes
router.use(express.json());

router.get("/:page/:to_limit", raw(controller.getPaginage));
router.get("/", raw(controller.getAllUsers));
router.get("/:id", raw(controller.getUserById));
router.put("/:id", raw(SchemaValidation),raw(controller.updateUserById));
router.delete("/:id", raw(controller.deleteUserById));
router.post("/", raw(SchemaValidation), raw(controller.createUser));
export default router;

// CREATES A NEW USER
// router.post("/", raw( async (req, res) => {
//     const value = await schema.validateAsync(req.body);
//     const user = await user_model.create(value);
//     res.status(200).json(user);
// }) );
